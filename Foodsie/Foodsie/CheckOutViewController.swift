//
//  CheckOutViewController.swift
//  Foodsie
//
//  Created by MAC MINI on 14/10/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController , UITextFieldDelegate {
    
   //View Outlets
    @IBOutlet weak var deliveryDetailsView: UIView!
    @IBOutlet weak var personalDetailsView: UIView!
    
    //Text Fields Outlets
    @IBOutlet weak var buildingtxtField: UITextField!
    @IBOutlet weak var streettxtField: UITextField!
    @IBOutlet weak var areatxtField: UITextField!
    @IBOutlet weak var firstnametxtField: UITextField!
    @IBOutlet weak var lastnametxtField: UITextField!
    @IBOutlet weak var contactnotxtField: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deliveryDetailsView.layer.borderColor = UIColor.lightGray.cgColor
        
        deliveryDetailsView.layer.borderWidth = 1
        
        personalDetailsView.layer.borderColor = UIColor.lightGray.cgColor
        personalDetailsView.layer.borderWidth = 1
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
        return true
    }
    
    //PlaceOrder Button Function
    
    @IBAction func placeOrder(_ sender: Any) {
        
        //Validating Whether Text Fields Are Empty or Not
        if checkFieldsEmptyorNot() == false {
            self.performSegue(withIdentifier: "placeorder", sender: self)
        }
      
    }
    
    // Validation function
    
    func checkFieldsEmptyorNot() -> Bool {
        if let building = buildingtxtField.text , let street = streettxtField.text, let area = areatxtField.text , let firstName = firstnametxtField.text ,
            let lastName = lastnametxtField.text , let contactNo = contactnotxtField.text ,
            building.isEmpty || street.isEmpty || area.isEmpty || firstName.isEmpty || lastName.isEmpty || contactNo.isEmpty {
            
            let alert = UIAlertController(title: "Error", message: "Fields Cannot be empty", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            present(alert,animated: true,completion: nil)
            return true
        }
        return false
    }
    
}

// Extension for changing textfield border color and width
extension UITextField {
    open override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true
    }

}

