//
//  Meals.swift
//  Foodsie
//
//  Created by Abdul Rehman on 18/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class MealsViewController: UITableViewController {

    var trackindex : Int = 0
    
    // Initializing Meals of categories
    var burgerMeals : [Meal] = [
        Meal(mealName: "Steak And Cheese Burger", mealDesc: "Strips of rib eye steak, grilled and sauteed with onions, provolone cheese, aioli sauce served on a grilled bun", mealPrice: 670, mealImage: UIImage(named: "burger1")!,qty: 0, id: 0),
        Meal(mealName: "Double Mushroom Melt Burger", mealDesc: "2x100 grams patty, processed cheese, sauteed onions, sauteed mushrooms, bbq sauce and OMG sauce in a toasted bun", mealPrice: 550, mealImage: UIImage(named: "burger2")!, qty: 0, id: 1),
        Meal(mealName: "Classic Burger Single", mealDesc: "100 grams patty, processed cheese, onions, tomatoes, ketchup and OMG sauce on a toasted bun", mealPrice: 350, mealImage: UIImage(named: "burger3")!, qty: 0, id: 2),
    ]
    
    var pizzaMeals : [Meal] = [
        Meal(mealName: "Double Melt", mealDesc: "Our one of a kind premium crust made with a thick layer of finest quality cream chinese sandwiched between two crunchy thin crusts", mealPrice: 1799, mealImage: UIImage(named: "pizza1")!, qty: 0, id: 3),
        Meal(mealName: "Pan", mealDesc: "Enjoy a soft & cheesy crust with 2 layers of mozzarella kneaded with butter chip", mealPrice: 1299, mealImage: UIImage(named: "pizza2")!, qty: 0, id: 4),
        Meal(mealName: "Hand Tossed", mealDesc: "Our signature oil-free crust hand stretched to perfection", mealPrice: 499, mealImage: UIImage(named: "pizza3")!, qty: 0, id: 5),
        ]

    var pastaMeals : [Meal] = [
        Meal(mealName: "Chicken Pasta", mealDesc: "Boil chicken, ginger, carrots,butter,capsicum", mealPrice: 1700, mealImage: UIImage(named: "pasta")!, qty: 0, id: 6),
        Meal(mealName: "Alfredo Pasta", mealDesc: "Mushrooms simmer in a creamy Alfredo sauce with tortellini pasta and chunks of chicken", mealPrice: 1400, mealImage: UIImage(named: "pasta2")!, qty: 0, id: 7),
        Meal(mealName: "Chicken Spaghetti", mealDesc: "Sharp cheddar cheese, chicken broth, green bell pepper", mealPrice: 500, mealImage: UIImage(named: "pasta3")!, qty: 0, id: 8)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

 
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "meal", for: indexPath) as! mealCell
    // If Burger Category is Selected
        if trackindex == 0 {
            let meals = burgerMeals[indexPath.row]
            cell.mealNamelbl.text = meals.mealName
            cell.mealDesclbl.text = meals.mealDesc
            cell.mealImg.image = meals.mealImage
            cell.mealPricelbl.text = String(meals.mealPrice)
            
            cell.mealImg.layer.cornerRadius = 5
            cell.mealImg.layer.masksToBounds = true
            
        }
        // If Pasta Category is Selected
        else if trackindex == 1 {
            let meals = pastaMeals[indexPath.row]
            cell.mealNamelbl.text = meals.mealName
            cell.mealDesclbl.text = meals.mealDesc
            cell.mealImg.image = meals.mealImage
            cell.mealPricelbl.text = String(meals.mealPrice)
            
            cell.mealImg.layer.cornerRadius = 5
            cell.mealImg.layer.masksToBounds = true
        }
        // If Pizza Category is Selected
        else if trackindex == 2 {
            let meals = pizzaMeals[indexPath.row]
            cell.mealNamelbl.text = meals.mealName
            cell.mealDesclbl.text = meals.mealDesc
            cell.mealImg.image = meals.mealImage
            cell.mealPricelbl.text = String(meals.mealPrice)
            
            cell.mealImg.layer.cornerRadius = 5
            cell.mealImg.layer.masksToBounds = true
        }
        return cell
    }
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Making Segue so we can access "selected" view controller properties
        guard let mealViewController = segue.destination as? MealDetailViewController,
            // Saving selected row number of table into index variable
            let index = tableView.indexPathForSelectedRow?.row
            else {
                return
        }
        // Passing selected Burger Meal Data
        if trackindex == 0 {
        mealViewController.Meals = burgerMeals[index]
        }
        // Passing selected Pasta Meal Data
        else if trackindex == 1 {
            mealViewController.Meals = pastaMeals[index]
        }
        // Passing selected Pizza Meal Data
        else if trackindex == 2 {
            mealViewController.Meals = pizzaMeals[index]
        }
        
    }
    
}

//TableView Cell Declaration
class mealCell : UITableViewCell {
    
    @IBOutlet weak var mealNamelbl: UILabel!
    @IBOutlet weak var mealDesclbl: UITextView!
    @IBOutlet weak var mealPricelbl: UILabel!
    @IBOutlet weak var mealImg: UIImageView!
}

