//
//  ViewController.swift
//  Foodsie
//
//  Created by Abdul Rehman on 14/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class CategoryTableViewController: UITableViewController {

    // Initiliazing Categories
    var category : [Category] = [
        Category(name: "Burger", image: UIImage(named: "burger")!),
        Category(name: "Pasta", image: UIImage(named: "pasta")!),
        Category(name: "Pizza", image: UIImage(named: "pizza")!)
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "category", for: indexPath) as! categoryCell
        
        //Accessing category values one by one
        let categories = category[indexPath.row]
        
        //Loading values into view.
        cell.photoCategory.image = categories.image
        cell.labelText.text = categories.name
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Making Segue so we can access "selected" view controller properties
        guard let MealViewController = segue.destination as? MealsViewController,
            // Saving selected row number of table into index variable
            let index = tableView.indexPathForSelectedRow?.row
            else {
                return
        }
        // Accessing trackindex variable of MealsVieWController
        MealViewController.trackindex = index        
    }
    
    
}

//TableView Cell Declaration
class categoryCell : UITableViewCell {
    @IBOutlet weak var photoCategory: UIImageView!
    @IBOutlet weak var labelText: UILabel!
}
