//
//  PlaceOrderViewController.swift
//  Foodsie
//
//  Created by MAC MINI on 14/10/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit

class PlaceOrderViewController: UIViewController {

    @IBOutlet weak var orderLblTxt: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Random Number Generate
        let number = Int.random(in: 0..<999)
        
        // Setting Order Label Field
        orderLblTxt.text = "FDSIE" + String(number)
        
        //Calling Local Notfication
        scheduleNotification()
        
    }
    
//Notification Schedule Function
    func scheduleNotification(){
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = "Order Confirmation"
        content.body = "Your Order has been placed"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request)
        
    }

}
