//
//  CartTableViewController.swift
//  Foodsie
//
//  Created by Abdul Rehman on 05/10/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit
// Delegate Initialize
protocol cartCountDelgate : class {
    func rtnCartCount(cartCount : Int)
}

class CartTableViewController: UITableViewController {
    //Outlets Declaration
    @IBOutlet weak var TotalPriceView: UIView!
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    //Variables Declaration
    var Mealscart : Meal?
    var totalqty : Int = 0
    var totalPrice : Int = 0
    var qtyData : Int = 0
    
    //Delegate Variable Declaration
    weak var delegate : cartCountDelgate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Calling Empty Message Function if cart is empty and hiding view
        if cartItem.cartItems.cartObj.isEmpty {
            self.tableView.setEmptyMessage("Cart is Empty")
            TotalPriceView.isHidden = true
    
        } else {
            // if cart is not empty then show data
            self.tableView.restore()
            TotalPriceView.isHidden = false
        }
        
        return cartItem.cartItems.cartObj.count
        
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cart", for: indexPath) as! cartCell
            // Accessing CartItem array object one by one
            let item = cartItem.cartItems.cartObj[indexPath.row]
       
            // Loading values into view
            cell.mealNameLbl.text = item.mealName
            cell.priceLbl.text = String(item.mealPrice)
            cell.qtyLbl.text = String(item.qty)
            totalPrice += item.mealPrice
            totalPriceLbl.text = String(totalPrice)
        
        //Calculating total cart items
        qtyData = cartItem.cartItems.cartObj.reduce(0) {$0 + $1.qty}
        
        //sending total qty data to previous controllers
        delegate?.rtnCartCount(cartCount:qtyData)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        //deleting data from array
        cartItem.cartItems.cartObj.remove(at: indexPath.row)
        
        //deleting row from table view
        tableView.deleteRows(at: [indexPath], with: .fade)
        
        //Calculating total cart items
        qtyData = cartItem.cartItems.cartObj.reduce(0) {$0 + $1.qty}
        
        //sending total qty data to previous controllers
        delegate?.rtnCartCount(cartCount:qtyData)
        
        
    }
    
    
}

//TableView Cell Declaration
class cartCell : UITableViewCell {
    
    @IBOutlet weak var qtyLbl: UILabel!
    @IBOutlet weak var mealNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
}

extension UITableView {
    //If cart is empty then this function will  be called
    func setEmptyMessage(_ message: String) {
        //creating new variable
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
        messageLabel.sizeToFit()
        
        //showing this label into view
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    //If cart is not empty then this function will  be called
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
