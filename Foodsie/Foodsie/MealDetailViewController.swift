//
//  MealDetailViewController.swift
//  Foodsie
//
//  Created by Abdul Rehman on 20/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit



class MealDetailViewController: UIViewController{
   //Outlets Declaration
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var mealImg: UIImageView!
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var mealDesc: UITextView!
    @IBOutlet weak var mealPrice: UILabel!
    @IBOutlet weak var itemLbl: UILabel!
   
    //Variables Declaration
    var qtyItem : Int = 0
    var badgecount: Int = 0
    var Meals : Meal?
    var label : UILabel!
    var count : Int = 0
    var totalPrice : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Changing view Properties
        cartView.layer.borderWidth = 2
        cartView.layer.borderColor = UIColor.gray.cgColor
        cartView.layer.cornerRadius = 12
        
        // Loading values into view
        mealImg.image = Meals?.mealImage
        mealName.text = Meals?.mealName
        mealDesc.text = Meals?.mealDesc
        mealPrice.text = String(Meals!.mealPrice)
        
        // Calling Badgecount function
        self.setupBadgeCount()
    }
    
   
    @IBAction func decItem(_ sender: Any) {
        if Meals!.qty > 0 {
            Meals!.qty = Meals!.qty - 1
        }
        itemLbl.text = String(Meals!.qty)
    }
    

    @IBAction func IncItem(_ sender: Any) {
        Meals!.qty = Meals!.qty + 1
        itemLbl.text = String(Meals!.qty)
    }
    
        @IBAction func addtoCart(_ sender: Any) {
            
            if cartItem.cartItems.badgeCount < 0 {
                badgecount = Meals!.qty
                 label.text = "\(cartItem.cartItems.badgeCount)"
            }
            else {
                label.text = String(cartItem.cartItems.badgeCount +  Meals!.qty)
            }
            //Adding Item in cartArray if it doesn't exist already in array
            if checkItem(cartItems: Meals!) == false {
                cartItem.cartItems.cartObj.append(Meals!)
                firstTimePriceCalc(Item: Meals!)
            }
            
    }
    
    func firstTimePriceCalc(Item : Meal) {
        for (index ,var i) in cartItem.cartItems.cartObj.enumerated(){
            if Item.id == i.id {
        cartItem.cartItems.cartObj[index].mealPrice = Item.mealPrice * Item.qty
            }
        }
    }
    //Func for checking item exist in cart or not
    func checkItem(cartItems : Meal) -> Bool {
        for (index ,var i ) in cartItem.cartItems.cartObj.enumerated(){
            // if item already exists in cart then add its quantity only
            if cartItems.id == i.id {
                cartItem.cartItems.cartObj[index].qty = cartItem.cartItems.cartObj[index].qty + cartItems.qty
                cartItem.cartItems.cartObj[index].mealPrice = cartItem.cartItems.cartObj[index].mealPrice + (cartItems.mealPrice * cartItems.qty)
                
                return true
            }
            
        }
        // returning false if items doesn't exist in array already
        return false

    }
    
        func setupBadgeCount(){
            // creating new label
            label = UILabel(frame: CGRect(x: 10, y: -05, width: 25, height: 25))
            label.layer.borderColor = UIColor.clear.cgColor
            label.layer.borderWidth = 2
            label.layer.cornerRadius = label.bounds.size.height / 2
            label.textAlignment = .center
            label.layer.masksToBounds = true
            label.textColor = .white
            label.font = label.font.withSize(12)
            label.backgroundColor = .red
            label.text = "\(cartItem.cartItems.badgeCount)"
            
            // creating button
            let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            rightButton.setBackgroundImage(UIImage(named: "shopping_cart"), for: .normal)
            rightButton.addTarget(self, action: #selector(self.cartclick(_:)), for: .touchUpInside)
            rightButton.addSubview(label)
            
            // creating Bar button item
            let rightBarButtomItem = UIBarButtonItem(customView: rightButton)
            navigationItem.rightBarButtonItem = rightBarButtomItem
        }
    
        @objc func cartclick(_ sender : UIButton) {
            // When Cart Icon is clicked navigate user to cart screen
            self.performSegue(withIdentifier: "cart", sender: self)
            }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cart" {
            let destinationController = segue.destination as! CartTableViewController
            destinationController.delegate = self
        }
    
}
    
    }

// Backward Data Access of Delegate
extension MealDetailViewController : cartCountDelgate{
    func rtnCartCount(cartCount: Int) {
        
        cartItem.cartItems.badgeCount = cartCount
        label.text = String(cartItem.cartItems.badgeCount)
        Meals!.qty = 0
        itemLbl.text = String(0)
    }
    
    
}
   


