//
//  Category.swift
//  Foodsie
//
//  Created by Abdul Rehman on 18/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import Foundation
import UIKit

struct Category {
    let name : String
    let image : UIImage
}

struct Meal {
    var mealName : String
    var mealDesc : String
    var mealPrice : Int
    var mealImage : UIImage
    var qty : Int = 0
    var id : Int = 0
}

struct cartItem {
    // Singleton variable i.e (can be accessed on any class)
    static var cartItems: cartItem = cartItem()
    
    var cartObj = [Meal]()
    var badgeCount : Int = 0
}


